package com.example.my.calculadora;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.math.BigDecimal;
import java.text.DecimalFormat;

public class MainActivity extends AppCompatActivity {

    public static final char NONE = '0';
    public static final char ADDITION = '+';
    public static final char SUBTRACTION = '-';
    public static final char MULTIPLICATION = '*';
    public static final char DIVISION = '/';
    public static final char DECIMAL_SEPARATOR = '.';

    private TextView textViewInfo;

    private EditText editTextInput;

    private Double valueOne;
    private Double valueTwo;

    private char currentOperation = NONE;

    private void appendChar(Character c) {
        String value = editTextInput.getText().toString();
        if (c == DECIMAL_SEPARATOR) {
            if (value.isEmpty()) {
                value = "0";
            } else if (value.indexOf(c) >= 0) {
                return;
            }
        }
        textViewInfo.setText(valueOne == null || currentOperation == NONE ? "" : valueOne.toString());
        editTextInput.setText(value + c);
    }

    private void prepareOperation(Character operation) {
        String value = editTextInput.getText().toString();
        if (valueOne == null && value.isEmpty()) {
            this.currentOperation = NONE;
            textViewInfo.setText("");
            return;
        }

        if (valueOne == null) {
            valueOne = Double.valueOf(value);
            textViewInfo.setText(valueOne.toString());
            editTextInput.setText("");
            this.currentOperation = operation;
            return;
        }

        if (value.isEmpty()) {
            this.currentOperation = operation;
            return;
        } else {
            valueTwo = Double.valueOf(value);
        }

        switch (this.currentOperation) {
            case ADDITION :
                valueOne += valueTwo;
                textViewInfo.setText(valueOne.toString());
                break;
            case SUBTRACTION :
                valueOne -= valueTwo;
                textViewInfo.setText(valueOne.toString());
                break;
            case MULTIPLICATION:
                valueOne *= valueTwo;
                textViewInfo.setText(valueOne.toString());
                break;
            case DIVISION:
                if (valueTwo.equals(Double.valueOf(0.0))) {
                    textViewInfo.setText(R.string.err_division);
                    editTextInput.setText("");
                    valueOne = null;
                } else {
                    valueOne /= valueTwo;
                    textViewInfo.setText(valueOne.toString());
                }
                break;
            default: // NONE
                break;
        }
        this.currentOperation = operation;
        editTextInput.setText("");
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        textViewInfo = findViewById(R.id.textViewInfo);
        editTextInput = findViewById(R.id.editTextInput);
        Button btnZero = findViewById(R.id.buttonZero);
        Button btnOne = findViewById(R.id.buttonOne);
        Button btnTwo = findViewById(R.id.buttonTwo);
        Button btnThree = findViewById(R.id.buttonThree);
        Button btnFour = findViewById(R.id.buttonFour);
        Button btnFive = findViewById(R.id.buttonFive);
        Button btnSix = findViewById(R.id.buttonSix);
        Button btnSeven = findViewById(R.id.buttonSeven);
        Button btnEight = findViewById(R.id.buttonEight);
        Button btnNine = findViewById(R.id.buttonNine);
        Button btnDot = findViewById(R.id.buttonDot);
        Button btnAdd = findViewById(R.id.buttonAdd);
        Button btnSub = findViewById(R.id.buttonSub);
        Button btnMul = findViewById(R.id.buttonMul);
        Button btnDiv = findViewById(R.id.buttonDiv);
        Button btnClean = findViewById(R.id.buttonClean);
        Button btnEqual = findViewById(R.id.buttonEquals);

        btnZero.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                appendChar('0');
            }
        });
        btnOne.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                appendChar('1');
            }
        });
        btnTwo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                appendChar('2');
            }
        });
        btnThree.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                appendChar('3');
            }
        });
        btnFour.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                appendChar('4');
            }
        });
        btnFive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                appendChar('5');
            }
        });
        btnSix.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                appendChar('6');
            }
        });
        btnSeven.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                appendChar('7');
            }
        });
        btnEight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                appendChar('8');
            }
        });
        btnNine.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                appendChar('9');
            }
        });
        btnDot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                appendChar(DECIMAL_SEPARATOR);
            }
        });
        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                prepareOperation(ADDITION);
            }
        });
        btnSub.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                prepareOperation(SUBTRACTION);
            }
        });
        btnMul.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                prepareOperation(MULTIPLICATION);
            }
        });
        btnDiv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                prepareOperation(DIVISION);
            }
        });
        btnClean.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                valueOne = null;
                textViewInfo.setText("");
                editTextInput.setText("");
            }
        });
        btnEqual.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                prepareOperation(NONE);
            }
        });
    }
}
